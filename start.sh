#!/bin/sh

source venv/bin/activate

sleep 5

export FLASK_APP=run_api.py

exec gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - run_api:app
