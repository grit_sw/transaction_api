FROM alpine:latest

ENV FLASK_APP run_api.py


WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 librdkafka-dev postgresql-libs
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    python3-dev \
    postgresql-dev \
    libffi-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

# COPY migrations migrations
COPY config.py logger.py run_api.py start.sh ./

ENV FLASK_ENV Staging
#ENV SECRET_KEY something
#ENV DATABASE_URL postgresql+psycopg2://ubuntu@localhost:5432/txe
RUN chmod +x start.sh

EXPOSE 5000
COPY api api
ENTRYPOINT ["./start.sh"]
