from flask import request

from .models import TransactionModel
from logger import logger


class TransactionManager(object):
	def create(self, payload):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		buyer_id = payload.get('buyer_id')
		seller_id = payload.get('seller_id')
		estimated_value = payload.get('estimated_value')

		new_transaction = TransactionModel.create(buyer_id=buyer_id, seller_id=seller_id, estimated_value=estimated_value)
		response['success'] = True
		response['data'] = new_transaction
		return response, 200

	def get_one(self, transaction_id):
		response = {}

		role_id = request.cookies.get('role_id')

		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		transaction = TransactionModel.get_one(transaction_id=transaction_id)

		if not transaction:
			response['success'] = False
			response['message'] = f'Transaction with id {transaction_id} not found.'
			return response, 404

		response['success'] = True
		response['data'] = transaction
		return response, 201

	def update(self, transaction_id, actual_value):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthorized.'
			return response, 403

		transaction = TransactionModel.update(transaction_id=transaction_id, actual_value=actual_value)
		if not transaction:
			response['success'] = False
			response['message'] = f'Transaction with id {transaction_id} was not updated, because it was not found.'
			return response, 404

		response['success'] = True
		response['data'] = transaction
		return response, 201

	def get_all(self):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthorised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthorised.'
			return response, 403

		all_transactions = TransactionModel.get_all()

		if not all_transactions:
			response['success'] = True
			response['data'] = 'No transactions found'
			return response, 404

		response['success'] = True
		response['data'] = all_transactions
		return response, 201
