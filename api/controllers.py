from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from .manager import TransactionManager
from .schema import NewTransactionSchema, EditTransactionSchema
from logger import logger

transaction_api = Namespace('transaction_api', description='Api for managing transactions')

new_transaction = transaction_api.model('NewTransaction', {
	'buyer_id': fields.String(required=True, description='The user id of buyer'),
	'seller_id': fields.String(required=True, description='The user id of seller'),
	'estimated_value': fields.Integer(required=True, description='Estimated amount in kobo of transaction'),
})

edit_transaction = transaction_api.model('EditTransaction', {
	'actual_value': fields.Integer(reqired=True, description='Actual value of transaction in kobo'),
})


@transaction_api.route('/new/')
class NewCircuitType(Resource):
	"""
		Api for creating a new transaction.
	"""

	@transaction_api.expect(new_transaction)
	def post(self):
		"""
			HTTP method for creating a new transaction.
			@param: payload
			@returns: response and status code
		"""
		schema = NewTransactionSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = dict()
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = TransactionManager()
		resp, code = manager.create(new_payload)
		return resp, code

@transaction_api.route('/edit/<int:transaction_id>')
class EditTransaction(Resource):
	"""
	Api for editing a transaction.
	"""
	@transaction_api.expect(edit_transaction)
	def put(self, transaction_id):
		"""
			HTTP method for editing a transaction.
			@param: payload
			@returns: response and status code
		"""
		schema = EditTransactionSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = dict()
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = TransactionManager()
		resp, code = manager.update(transaction_id=transaction_id, actual_value=new_payload.get('actual_value'))
		return resp, code


@transaction_api.route('/one/<int:transaction_id>/')
class OneCircuitType(Resource):
	"""
		Api for viewing a transaction.
	"""

	@staticmethod
	def get(transaction_id):
		"""
			HTTP method for viewing a transaction.
			@param: payload
			@returns: response and status code
		"""
		manager = TransactionManager()
		resp, code = manager.get_one(transaction_id=int(transaction_id))
		return resp, code


@transaction_api.route('/all/')
class AllCircuitTypes(Resource):
	"""
		Api for viewing all transactions.
	"""
	@staticmethod
	def get():
		"""
			HTTP method for viewing all  transactions.
			@param: payload
			@returns: response and status code
		"""
		manager = TransactionManager()
		resp, code = manager.get_all()
		return resp, code
