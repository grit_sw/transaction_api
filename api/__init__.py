from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api

from config import config


db = SQLAlchemy()
api = Api(doc='/doc/')

app = Flask(__name__)

def create_api(config_name):
	try:
		init_config = config[config_name]()
	except KeyError:
		raise
	except Exception:
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)
	config_object.init_app(app)

	db.init_app(app)

	from api.controllers import transaction_api as ns1
	api.add_namespace(ns1, path='/transaction-api')
	api.init_app(app)

	with app.app_context():
		db.create_all()

	return app
