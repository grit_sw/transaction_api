import arrow

from api import db


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(
		db.DateTime,
		default=db.func.current_timestamp(),
		onupdate=db.func.current_timestamp()
	)

	def get_data(self):
		return self.to_dict()


class TransactionModel(Base):
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	buyer_id = db.Column(db.String(128), nullable=False)
	seller_id = db.Column(db.String(128), nullable=False)
	estimated_value = db.Column(db.Integer, nullable=False)
	actual_value = db.Column(db.Integer, nullable=True)

	@staticmethod
	def create(buyer_id, seller_id, estimated_value):
		new_transaction = TransactionModel(buyer_id=buyer_id, seller_id=seller_id, estimated_value=estimated_value)
		db.session.add(new_transaction)
		db.session.commit()
		return new_transaction.to_dict()

	@staticmethod
	def get_one(transaction_id):
		transaction = db.session.query(TransactionModel).filter_by(id=transaction_id).first()
		try:
			return transaction.to_dict()
		except AttributeError:
			return None

	@staticmethod
	def update(transaction_id, actual_value):
		transaction = db.session.query(TransactionModel).filter_by(id=transaction_id).first()
		try:
			transaction.actual_value = actual_value
		except AttributeError:
			return None
		db.session.add(transaction)
		db.session.commit()
		return transaction.to_dict()

	@staticmethod
	def get_all():
		all_transactions = db.session.query(TransactionModel).all()
		try:
			all_transactions_lst = [transaction.to_dict() for transaction in all_transactions]
		except AttributeError:
			return None
		return all_transactions_lst

	def to_dict(self):
		response = {
			'id': self.id,
			'buyer_id': self.buyer_id,
			'seller_id': self.seller_id,
			'estimated_value': self.estimated_value,
			'actual_value': self.actual_value,
			'date_created': str(arrow.get(self.date_created)),# todo reformat to sth more readable
			'date_modified': str(arrow.get(self.date_modified))
		}
		return response
		# id , buyer_id, seller_id and actual value of this(response)  should be passed to order_id, buyer_id, seller_id and value  of the blockchain api

#
# def send_to_blockchain(transaction, action):
# 	#ideally transactions without an actual value are just been created and therefore have not been sent to the blockchain
# 	if not transaction.actual_value:
# 		blockchain_api_url = os.environ['BLOCKCHAIN_API_URI']
# 		new_order_url = f'{blockchain_api_url}/new-order'
# 		data = {'order_id': transaction.id,
# 				'buyer_id': transaction.buyer_id,
# 				'seller_id': transaction.seller_id,
# 				'value': transaction.estimated_value,
# 				'other_details': "" # todo include in model
# 				}
# 		res = requests.post(url=new_order_url, json=data)
# 		logger.info(res.json())
#
# def before_commit_listener(session):
# 	session._changes = {
# 		'add': list(session.new),
# 		'update': list(session.dirty),
# 		'delete': list(session.deleted)
# 	}
#
#
# def after_commit_listener(session):
# 	for obj in session._changes['add']:
# 		if isinstance(obj, TransactionModel):
# 			send_to_blockchain(obj, action="saved")
# 	for obj in session._changes['update']:
# 		if isinstance(obj, 	TransactionModel):
# 			send_to_blockchain(obj, action='updated')
# 	for obj in session._changes['delete']:
# 		if isinstance(obj, TransactionModel):
# 			print('Users to be deleted')
#
#
# db.event.listen(db.session, 'before_commit', before_commit_listener)
# db.event.listen(db.session, 'after_commit', after_commit_listener)
#todo attach blockchain params async. through kafka, using the after flush event,
# since the db is not changed it is okay.