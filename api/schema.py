from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates


NewTransaction = namedtuple('Transaction', [
	'buyer_id',
	'seller_id',
	'estimated_value',
	])

EditTransaction = namedtuple('EditTransaction', [
	'actual_value'
])


class NewTransactionSchema(Schema):
	buyer_id = fields.String(required=True)
	seller_id = fields.String(required=True)
	estimated_value = fields.Integer(required=True)

	@post_load
	def create_transaction(self, data):
		return NewTransaction(**data)

	@validates('estimated_value')
	def validate_name(self, value):
		try:
			int(value)
		except ValueError:
			raise ValidationError(f'Could not convert {value} to integer')
		except TypeError:
			raise ValidationError(f'Estimated value:{value} must be a integer, not {type(value)}')


class EditTransactionSchema(Schema):
	actual_value = fields.Integer(required=True)

	@post_load
	def edit_transaction(self, data):
		return EditTransaction(**data)

	@validates('actual_value')
	def validate_name(self, value):
		try:
			int(value)
		except ValueError:
			raise ValidationError(f'Could not convert {value} to integer')
		except TypeError:
			raise ValidationError(f'Actual value:{value} must be an integer, not {type(value)}')
