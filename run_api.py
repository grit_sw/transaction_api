from os import environ

from arrow import now
from flask import request

from api import create_api
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'

app = create_api(mode)

# @app.before_first_request
# def setup():
# 	from api.models import CircuitTypeModel, SubnetTypeModel, CurrentTypeModel
# 	CircuitTypeModel.setup()
# 	SubnetTypeModel.setup()
# 	CurrentTypeModel.setup()
# 	logger.info(CircuitTypeModel.get_all())
# 	logger.info(SubnetTypeModel.get_all())
# 	logger.info(CurrentTypeModel.get_all())


@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location,
		}
		logger.info('transaction_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("transaction_api_logs: {}".format(e))
	return response
