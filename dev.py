from pathlib import Path
from dotenv import load_dotenv
import subprocess
import sys


env_path = Path('.') / '.flaskenv'
load_dotenv(dotenv_path=env_path)

from api import app

@app.cli.command()
def dev():
    """Run development server."""
    dev = 'gunicorn -b :5000 -k eventlet -t 2000 run_api:app'.split(" ")
    ret = subprocess.call(dev)
    sys.exit(ret)

